package com.teamtreehouse.contactmgr;

import com.teamtreehouse.contactmgr.model.Contact;
import com.teamtreehouse.contactmgr.service.ContactService;
import com.teamtreehouse.contactmgr.service.impl.ContactServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;

public class Application implements Runnable{
    private static final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        new Application(new ContactServiceImpl()).run();
    }

    private ContactService contactService;

    public Application(ContactService contactService) {
        this.contactService = contactService;
    }

    @Override
    public void run() {
        out.printf("WELCOME TO THE TREEHOUSE CONTACT MANAGER%n%n");

        while(true){
            // Get menu option
            int option = getMenuOption();
            out.println();

            // Exit if user chose to
            if(option == 4) System.exit(0);

            // Process menu option
            process(option);
        }
    }

    private void process(int option) {
        switch (option) {
            case 1: // View contacts
                listContacts();
                break;
            case 2: // Add contact
                addContact();
                break;
            case 3: // Delete contact
                deleteContact();
                break;
            case 4:
            default:
                break;
        }
    }

    private void listContacts() {
        List<Contact> allContacts = contactService.findAll();
        out.printf("Showing %s contacts%n%n", allContacts.size());
        for(Contact contact : allContacts) {
            out.println("\t" + contact);
        }
        out.println();
    }

    private void addContact() {

        String firstName;
        String lastName;
        while(true) {
            out.print("Enter the contact's name: ");
            try {
                String name = in.readLine();
                String[] parts = name.split(" ");

                // If a first and last name weren't both entered
                if(parts.length != 2) {
                    out.printf("Please enter both a first and last name.%n%n");
                    continue;
                } else {
                    firstName = parts[0];
                    lastName = parts[1];
                    break;
                }
            } catch (IOException e) {}
        }
        Contact contact = new Contact(lastName,firstName);
        contactService.save(contact);
    }

    private void deleteContact() {

    }

    private int getMenuOption() {
        int option = 0;
        while(true) {
            menu();
            try {
                option = Integer.parseInt(in.readLine());
                if(Arrays.asList(1,2,3,4).contains(option)) {
                    return option;
                } else {
                    out.printf("Invalid option.%n");
                }
            } catch (NumberFormatException|IOException e) {
                out.printf("Invalid option.%n");
            }
        }
    }

    private void menu() {
        out.printf("Please choose from on of the following menu options:%n");
        out.printf("\t1 View contacts%n\t2 Add contact%n\t3 Delete contact%n\t4 Exit%nYour choice: ");
    }
}