package com.teamtreehouse.contactmgr.service.impl;

import com.teamtreehouse.contactmgr.model.Contact;
import com.teamtreehouse.contactmgr.service.ContactService;
import com.teamtreehouse.contactmgr.service.DataService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ContactServiceImpl extends DataService<Contact> implements ContactService {
    @Override
    public Contact findById(Long id) {
        Map<String,Object> properties = new HashMap<>();
        properties.put("id",id);
        List<Contact> matches = findByProperties(properties);
        if(matches != null && matches.size() > 0) {
            return matches.get(0);
        }
        return null;
    }

    @Override
    public Class<Contact> getEntityType() {
        return Contact.class;
    }
}
