package com.teamtreehouse.contactmgr.service;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

public abstract class DataService<E> {
    private static final SessionFactory SESSION_FACTORY = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        //try {

            // Create Hibernate configuration
            //Configuration configuration = new Configuration();
            //configuration.addAnnotatedClass(Contact.class);
            //ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().loadProperties("hibernate.properties").build();

            return new MetadataSources(registry).buildMetadata().buildSessionFactory();
       // } catch (Exception e) {
            // Log the exception
            //System.err.printf("Initial SessionFactory creation failed: %s%n", e);
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            //StandardServiceRegistryBuilder.destroy(registry);
            //return null;
        //}
    }

    public static SessionFactory getSessionFactory() {
        return SESSION_FACTORY;
    }

    // CRUD operations

    public void save(E entity) {
        try {
            Session session = getSessionFactory().openSession();
            session.beginTransaction();
            session.saveOrUpdate(entity);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            System.err.printf("Save failed: %s%n", e);
        }
    }

    public void delete(E entity) {
        try {
            Session session = getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            System.err.printf("Delete failed: %s%n", e);
        }
    }

    public List<E> findAll() {
        return findByProperties(null);
    }

    @SuppressWarnings("unchecked")
    public List<E> findByProperties(Map<String, Object> properties) {
        Session session = getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(getEntityType());
        if (properties != null) {
            for (String key : properties.keySet()) {
                criteria.add(Restrictions.eq(key, properties.get(key)));
            }
        }
        List<E> matches = criteria.list();
        session.close();
        return matches;
    }

    public abstract Class<E> getEntityType();
}
